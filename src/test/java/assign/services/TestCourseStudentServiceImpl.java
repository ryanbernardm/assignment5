package assign.services;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import assign.domain.Project;

public class TestCourseStudentServiceImpl {
	
	ProjectService projectService = null;
        Logger testLogger = Logger.getLogger("testlogger");
	
	@Before
	public void setUp() {
		String dburl = "jdbc:mysql://localhost:3306/student_courses";
		String dbusername = "root";
		String dbpassword = "Goodserver1";
		projectService = new ProjectServiceImpl();
	}
	
	@Test
	public void testCourseAddition() {
		try {
			Project c = new Project();
			c.setName("Introduction to Computer Science.");
			c.setProjectId(102);
			c = projectService.addProject(c);
			
			Project c1 = projectService.getProject(c.getProjectId());
			
			assertEquals(c1.getName(), c.getName());
			assertEquals(c1.getProjectId(), c.getProjectId());
			assertEquals(c1.getProjectId(), c.getProjectId());			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    @Test
	public void testCourseGet() {
	try {
	    Project c = new Project();
	    c.setName("Introduction to Computer Science.");
	    c.setProjectId(102);
	    c = projectService.addProject(c);
	    
	    Project c1 = projectService.getProject(c.getProjectId());
	    testLogger.info(c1.getName());
	    testLogger.info(Integer.toString(c1.getProjectId()));
	    testLogger.info(String.valueOf(c1.getProjectId()));
	    assertEquals(c1.getName(), c.getName());
	    assertEquals(c1.getProjectId(), c.getProjectId());
	    assertEquals(c1.getProjectId(), c.getProjectId());
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}