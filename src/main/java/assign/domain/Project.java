package assign.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table( name = "project" )
@XmlRootElement(name="project")
@XmlType(propOrder={"name","description","meetings"})
public class Project {

	private String project_name;
	private int projectId;
	private String description;
	private Set<Meeting> meetings;
	
	public Project(){
		
	}

	public Project(String name, String description){
		this.project_name = name;
		this.description = description;
	}
	
	@Column(name = "project_name")
	@XmlElement
	public String getName() {
		return project_name;
	}
	
	public void setName(String name) {
		this.project_name = name;
	}
	
    @Id
   	@GeneratedValue(generator="increment")
   	@GenericGenerator(name="increment", strategy = "increment")
    @XmlAttribute(name="id")
    public int getProjectId(){
    	return projectId;
    }
    
    public void setProjectId(int projectId){
    	this.projectId= projectId;
    }


	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement////////////////////////////////
	@Column(name = "description")
	public String getDescription(){
		return description;
	}
	
    @OneToMany(mappedBy="project")
    @Cascade({CascadeType.DELETE})
	@XmlElementWrapper
	@XmlElement(name="meeting")
    public Set<Meeting> getMeetings() {
    	return this.meetings;
    }
    
    public void setMeetings(Set<Meeting> meetings) {
    	this.meetings = meetings;
    }
	
}
