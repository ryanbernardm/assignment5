package assign.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.*;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;

@Entity
@Table(name="meeting")
@XmlRootElement(name="meeting")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meeting {
	@XmlAttribute(name="id")
	int meeting_id;
	String name;
	String year;
	@XmlTransient
	Project proj;
	
	public Meeting() {
		
	}
	
	public Meeting(String name, String year) {
		this.name = name;
		this.year = year;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	//@XmlAttribute(name="id")
	@Column (name = "meeting_id")
	public int getMeetingId() {
		return meeting_id;
	}
	
	public void setMeetingId(int id) {
		this.meeting_id = id;
	}
	
	//@XmlElement(name="name")
	@Column(name="meeting_name")
	public String getMeetingName() {
		return name;
	}
	
	public void setMeetingName(String name) {
		this.name = name;
	}
	
	//@XmlElement(name="year")
	@Column(name="year")
	public String getMeetingYear() {
		return year;
	}
	
	public void setMeetingYear(String year) {
		this.year = year;
	}
	
	@ManyToOne
	@JoinColumn(name="project_id")
	public Project getProject() {
		return this.proj;
	}
	
	public void setProject(Project p) {
		this.proj = p;
	}
}