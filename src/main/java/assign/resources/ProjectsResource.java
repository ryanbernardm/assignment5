package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.ProjectService;
import assign.services.ProjectServiceImpl;

@Path("/projects")
public class ProjectsResource {
	
	ProjectService projectService;

	public ProjectsResource(@Context ServletContext servletContext) {
		
		this.projectService = new ProjectServiceImpl();
	}
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		return "helloworld";		
	}
	
	@POST
	@Path("/")
	@Consumes("application/xml")
	public Response createProject(InputStream is) throws SQLException {
	      Project newProject = readProject(is);
	      if(newProject.getName().trim().equals("")|| newProject.getDescription().trim().equals("")){
		        throw new WebApplicationException(Response.Status.BAD_REQUEST);
	      }
	      newProject = this.projectService.addProject(newProject);
	      return Response.created(URI.create("/myeavesdrop/projects/" + newProject.getProjectId())).build();
	}
	
	@GET
	@Path("/{project_id}")
	@Produces("application/xml")
	public Response getProject(@PathParam("project_id") Integer project_id, InputStream is) throws Exception {
		final Project temp = projectService.getProject(project_id);
		if(temp == null){
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		System.out.println(temp.getMeetings().toString());

		StreamingOutput streamingOut = new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, temp);
	         }
		};
		return Response.status(Response.Status.OK).entity(streamingOut).build();
	}
	
	@DELETE
	@Path("/{project_id}")
	public void deleteProj(@PathParam("project_id") Integer id) throws Exception{
		if(projectService.getProject(id) == null){
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		projectService.deleteProject(id);
	}
	
	@POST
	@Path("/{project_id}/meetings/")
	@Consumes("application/xml")
	public Response createMeeting(@PathParam("project_id") final Integer project_id, InputStream is) throws Exception, NumberFormatException {
		Meeting newMeeting = readMeeting(is);
		if(newMeeting.getMeetingName().trim().equals("")){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
	    }
		try {
			Project temp = projectService.getProject(project_id);
			if(temp != null) {
				System.out.println("Found the project with given id");
				Meeting tempMeeting = projectService.addMeeting(newMeeting, temp);
				if(tempMeeting != null) {
					return Response.status(Response.Status.CREATED).build();
				}
			} else {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	
	@PUT
	@Path("/{project_id}/meetings/{meeting_id}")
	@Consumes("application/xml")
	public Response updateMeeting(@PathParam("project_id") final Integer project_id, @PathParam("meeting_id") final Integer meeting_id, InputStream is) throws Exception {
		Meeting meeting = readMeeting(is);
		if(meeting.getMeetingName().trim().equals("")){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
	    }
		try {
			Project tempProj = projectService.getProject(project_id);
			if(tempProj != null) { //maybe need for each loop to see if meeting is in Set
				for(Meeting meet : tempProj.getMeetings()){
					if(meet.getMeetingId() == meeting_id){
						meet.setMeetingName(meeting.getMeetingName());
						meet.setMeetingYear(meeting.getMeetingYear());
						projectService.updateMeetings(meet);
						return Response.status(Response.Status.OK).build();
					}
				}
			}
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected Project readProject(InputStream is) {
      try {
         DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
         Document doc = builder.parse(is);
         Element root = doc.getDocumentElement();
         Project project = new Project();
         NodeList nodes = root.getChildNodes();
         for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);
            if (element.getTagName().equals("name")) {
               project.setName(element.getTextContent());
            }
            else if (element.getTagName().equals("description")) {
               project.setDescription(element.getTextContent());
            }
         }
         return project;
      }
      catch (Exception e) {
         throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
      }
   }
	
	protected Meeting readMeeting(InputStream is) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(is);
			Element root = doc.getDocumentElement();
			Meeting meeting = new Meeting();
			NodeList nodes = root.getChildNodes();
			for(int i = 0; i < nodes.getLength(); i++) {
				Element elem = (Element) nodes.item(i);
				if(elem.getTagName().equals("name")) {
					meeting.setMeetingName(elem.getTextContent());
				} else if(elem.getTagName().equals("year")) {
					String year = elem.getTextContent();
					if(validateYear(year)){
						meeting.setMeetingYear(year);
					} else {
						//System.out.println("Year validation failed.");
						throw new WebApplicationException(Response.Status.BAD_REQUEST);
					}
				}
			}
			
			return meeting;
		}
		catch(Exception e) {
			//System.out.println("something wrong with parsing.");
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		}
	}
	
	protected boolean validateYear(String in) {
		int year;
		try {
			 year = Integer.parseInt(in);
		}
		catch(NumberFormatException e) {
			return false;
		}
		if(year >= 2010 && year <= 2017) {
			return true;
		}
		else {
			return false;
		}
	}
}