package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Meeting;
import assign.domain.Project;

public class ProjectServiceImpl implements ProjectService {
	SessionFactory sessionFactory;
	Logger logger;

	@SuppressWarnings("deprecation")
	public ProjectServiceImpl() {
		sessionFactory = new Configuration().configure().buildSessionFactory();
		logger = Logger.getLogger("MyEavesdrop");
	}

	@Override
	public Project getProject(int projectId) throws Exception {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Project.class).add(Restrictions.eq("projectId", projectId));
		List<Project> projectList = criteria.list();
		
		if(projectList.size() > 0) {
			Hibernate.initialize(projectList.get(0).getMeetings());
			session.close();
			return projectList.get(0);
		} else {
			session.close();
			return null;
		}
	}

	@Override
	public Project addProject(Project newProject) throws SQLException {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.save(newProject);
			tx.commit();
		} catch(Exception e) {
			if(tx != null) {
				tx.rollback();
				throw e;
			}
		} finally {
			session.close();
		}
		
		return newProject;
	}
	
	@Override
	public void deleteProject(Integer project_id) throws Exception{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Project proj = getProject(project_id);
		session.delete(proj);
		session.getTransaction().commit();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Meeting addMeeting(Meeting meeting, Project project) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(Project.class).add(Restrictions.eq("id", project.getProjectId()));
			List<Project> projectList = criteria.list();
			meeting.setProject(projectList.get(0));
			//project.getMeetings().add(meeting);
			session.save(meeting);
			tx.commit();
		} catch(Exception e) {
			if(tx != null) {
				tx.rollback();
				throw e;
			}
		} finally {
			session.close();
		}
		
		return meeting;
	}
	
	@Override
	public Meeting getMeeting(Integer meetingId) throws Exception {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Meeting.class).add(Restrictions.eq("meetingId", meetingId));
		List<Meeting> meetingList = criteria.list();
		
		session.close();
		if(meetingList.size() > 0) {
			return meetingList.get(0);

		} else {
			return null;
		}
	}
	
	@Override
	public void updateMeetings(Meeting meeting) throws Exception {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(meeting);
		session.getTransaction().commit();
		session.close();
	}
}
