package assign.services;

import java.sql.SQLException;

import assign.domain.Meeting;
import assign.domain.Project;

public interface ProjectService {
	
	public Project getProject(int projectId) throws Exception;

	public Project addProject(Project newProject) throws SQLException;
	
	public void deleteProject(Integer projectId) throws SQLException, Exception;

	public Meeting addMeeting(Meeting meeting, Project project) throws Exception;
	
	public Meeting getMeeting(Integer meetingId) throws Exception;
	
	public void updateMeetings(Meeting meeting) throws Exception;
}
